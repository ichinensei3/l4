class UsersController < ApplicationController
    def index
        @users = User.all
    end
    def new
        @user = User.new
    end
    def create
         pass = BCrypt::Password.create(params[:user][:pass])
         @user = User.new(uid: params[:user][:uid], pass: pass)
        if @user.save
            
            flash[:success] = "登録完了"
            redirect_to users_path
        else
            flash[:failed] = "登録失敗"
            render 'new'
        end
    end
    def destroy 
        user = User.find(params[:id])
        user.destroy
            flash[:destroy] = "削除しました。"
        redirect_to users_path, flash: {destroy:  "ツイートを削除しました。"}
    end
end