class User < ApplicationRecord
    validates :uid, uniqueness: true
    def self.authenticate(uid,pass)
        if user = find_by(uid,pass:pass)
            user
        else
            nil
        end
    end
end
